import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.text.Font;

import java.util.Vector;

/**
 * La vue du jeu du taquin
 */
public class VueTaquin extends VBox {
    private Image image; // image servant de support au taquin
    private ModelTaquin taquin; // modèle du jeu de taquin
    private double largeurTuile; // largeur en nombre de pixels d'une tuile du taquin
    private double hauteurTuile; // hauteur en nombre de pixels d'une tuile du taquin
    private double tailleMax; // taille maximum autorisée pour l'affichage du taquin
    private Vector<Tuile> lesTuiles; // vecteur contenant les tuile du taquin
    private Label lNbMouvements; // label permettant d'indiquer le nombre de mouvements effectués

    /**
     * Constructeur de la vue du taquin.
     * @param taquin modèle du taquin
     * @param img image servant de support au taquin
     * @param tailleMax la taille maximum autorisée pour une dimension (hauteur ou largeur) de l'image du taquin
     */
    public VueTaquin(ModelTaquin taquin, Image img, double tailleMax) {
        super();
        // A IMPLEMENTER
    }

    /**
     * active ou désactive les tuiles du taquin
     * @param actif booléen indiquant l'état d'activité à mettre au taquin
     */
    void activer(boolean actif){
        // A IMPLEMENTER
    }

    /**
     * met à jour la vue du taquin en fonction du modèle
     */
    void update(){
        // A IMPLEMENTER
    }
}
