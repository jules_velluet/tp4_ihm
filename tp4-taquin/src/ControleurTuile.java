import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.input.MouseEvent;

/**
 * Contrôleur qui va être exécuter lorsque l'on clique sur une tuile du taquin
 */
public class ControleurTuile implements EventHandler<MouseEvent> {
    private ModelTaquin modelTaquin; // modèle du jeu de taquin
    private VueTaquin vueTaquin; // vue du jeu de taquin
    private int lig,col; // ligne et colonne où se situe la tuile dans la vue

    /**
     * constructeur
     * @param modelTaquin modèle du jeu de taquin
     * @param vueTaquin vue du jeu de taquin
     * @param lig ligne où se situe la tuile dans la vue
     * @param col colonne où se situe la tuile dans la vue
     */
    public ControleurTuile(ModelTaquin modelTaquin, VueTaquin vueTaquin, int lig, int col) {
        // A IMPLEMENTER
    }

    @Override
    public void handle(MouseEvent mouseEvent) {
        // A IMPLEMENTER
    }
}
