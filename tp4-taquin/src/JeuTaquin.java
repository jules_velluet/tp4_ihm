import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;


public class JeuTaquin extends Application {
    ModelTaquin taquin;
    Stage st;

    public static void main(String[] args) {
        launch(args);
    }

    void setSceneTuile(double largeur,double hauteur,double ratio, int lig, int col, boolean actif, Image image){
        Tuile tuile=new Tuile(largeur,hauteur,ratio,lig,col,image);
        tuile.activer(actif);
        FlowPane fp=new FlowPane(tuile);
        fp.setAlignment(Pos.CENTER);
        st.setScene(new Scene(fp));
    }
    void setSceneTaquin(Image img){
        VueTaquin vue=new VueTaquin(taquin,img,400);
        st.setScene(new Scene(vue));
    }

    @Override
    public void init() {taquin=new ModelTaquin(3,3);}
    @Override
    public void start(Stage stage) {
        st=stage;
        Image img=new Image("file:img/logo.jpg");
        stage.setTitle("Jeu du taquin");
        stage.setWidth(500);
        stage.setHeight(500);
        boolean actif=true; // mettre actif à false pour tester la méthode activer()
        setSceneTuile( img.getWidth()/3,img.getHeight()/3,1,1,0,actif, img);
        //setSceneTaquin(img);
        stage.show();
    }
}
